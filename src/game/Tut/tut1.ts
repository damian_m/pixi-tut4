import * as PIXI from 'pixi.js'
import cat from '../assets/images/cat.png'
import tilesetImg from '../assets/images/tileset.png'
//import cat from '../images/cat.png'
export default ()=> {

//Create a Pixi Application
let app = new PIXI.Application({ 
  width: 256,         // default: 800
  height: 256,        // default: 600
  antialias: true,    // default: false
  transparent: false, // default: false
  resolution: 1       // default: 1
});

//Add the canvas that Pixi automatically created for you to the HTML document
//console.log(cat)
document.getElementById("game").appendChild(app.view); 
//console.log('cat', cat)
app.renderer.backgroundColor = 0xFF2D00;
//console.log(cat)
const loader = new PIXI.Loader()
  loader.add("cat",cat);
  loader.add("tileset",tilesetImg);
  loader.on("progress", (loader, resource) => console.log('loading', loader, resource))
  loader.load(setup);

//This `setup` function will run when the image has loaded
  function setup() {

    //Create the cat sprite
    let catf = new PIXI.Sprite(loader.resources['cat'].texture);

    //////////////////POSITION
    catf.x = 96;
    catf.y = 96;
    //or 
    //catf.position.set(96,96)

    ///////////////SCALE
    catf.width = 80;
    catf.height = 120;
    //or
    // catf.scale.x = 0.5;
    // catf.scale.y = 0.5;
    //or
    //cat.scale.set(0.5, 0.5);

    /////////////////TILESET
    let tilesetTxt = PIXI.utils.TextureCache['tileset'];
    let rectangle = new PIXI.Rectangle(96, 64, 32, 32);

    //Tell the texture to use that rectangular section
    tilesetTxt.frame = rectangle;
  
    //Create the sprite from the texture
    let rocket = new PIXI.Sprite(tilesetTxt);
  
    //Position the rocket sprite on the canvas
    rocket.x = 32;
    rocket.y = 32;
  
    //Add the rocket to the stage
    app.stage.addChild(rocket);

    //Add the cat to the stage
    app.stage.addChild(catf);
  }
}


