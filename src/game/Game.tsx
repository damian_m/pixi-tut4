

import * as React from "react";
import tut from './Tut/tut1'
export interface IGameProps {}

export default class Game extends React.Component<IGameProps, any> {
    componentDidMount() {
      tut()
    }
  
    shouldComponentUpdate() { 
      return false;
    }
  
    public render() {
      return <div id="game" />;
    }
  }
  