import React, { Component } from "react";

import { Provider } from "react-redux";

import store from "./store";
import Game from "./game/Game";

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <div
          style={{
            display: "flex",
            alignContent: "center",
            justifyContent: "center",
            flexDirection: "row",
            height: "100vh"
          }}
        >
          {/* <UI /> */}
          <Game />
        </div>
      </Provider>
    );
  }
}

export default App;
